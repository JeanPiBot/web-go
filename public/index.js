// 1 es 🪨, 2 es 📝, 3 es ✂️
let jugador = 0;
let min = 1;
let max = 3;
let pc = 0;
let triunfos = 0;
let perdidas = 0;

function aleatorio(maximo, minimo) {
  return Math.floor(Math.random() * (maximo - minimo + 1) + minimo);
}

function eleccion(jugada) {
  let resultado = 0;
  if (jugada == 1) {
    resultado = "🪨";
  } else if (jugada == 2) {
    resultado = "📝";
  } else if (jugada == 3) {
    resultado = "✂️";
  } else {
    resultado = "No es un valor valido";
  }

  return resultado;
}

while (triunfos < 3 && perdidas < 3) {
  pc = aleatorio(max, min);

  jugador = prompt("Elige: 1 para 🪨, 2 para 📝 y 3 para ✂️");

  alert(`El pc eligio: ${eleccion(pc)}`);
  alert(`Tu elegiste: ${eleccion(jugador)}`);

  if (pc == jugador) {
    alert("Empate");
  } else if (
    (jugador == 1 && pc == 3) ||
    (jugador == 2 && pc == 1) ||
    (jugador == 3 && pc == 2)
  ) {
    alert("Ganaste");
    triunfos += 1;
  } else {
    alert("Perdiste");
    perdidas -= 1;
  }
}

alert(`Ganaste ${triunfos} veces y perdiste ${perdidas} veces`);
