# Rock, Paper, Scissors Game in Go

This is a simple Rock, Paper, Scissors game implemented in Go.

## Prerequisites

Make sure you have Go installed on your machine. You can download it from the official website: [https://golang.org/dl/](https://golang.org/dl/)

## Getting Started

1. Clone the repository to your local machine:

```bash
  git clone git clone git@gitlab.com:JeanPiBot/web-go.git
```

2. Navigate to the project directory:

```bash
  cd web-go
```

3. Run the game:

```bash
  go run main.go
```

## Contribution

If you wish to contribute to this project and help it grow, follow these steps:

1. Fork this repository.
2. Create a new branch with a descriptive name: git checkout -b branch_name.
3. Make the necessary modifications and commit your changes.
4. Push your changes to the remote repository: git push origin branch_name.
5. Open a pull request to review your changes and merge them into the main branch.

# Authors

- **Jean Pierre Giovanni Arenas Ortiz**

## License

This application is licensed under the [MIT License](https://opensource.org/licenses/MIT).
