package main

import (
	"fmt"
	"net/http"
)

func main() {

	server := http.Server{
		Addr: ":8081",
	}

	fileServer := http.FileServer(http.Dir("./public"))
	http.Handle("/", fileServer)
	fmt.Println("Server is running on port ", server.Addr)
	server.ListenAndServe()
}
